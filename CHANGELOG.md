# Forestish Color Theme - Change Log

## [0.2.3]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.2.2]

- update readme and screenshot

## [0.2.1]

- fix manifest and pub WF

## [0.2.0]

- fix manifest repo links

## [0.1.1]

- fix fold indicators:
editorGutter.foldingControlForeground: lighten indicators
- fix scrollbar, minimap slider transparencies
dark:
"minimapSlider.activeBackground": "#ffffff40",
"minimapSlider.background": "#ffffff20",
"minimapSlider.hoverBackground": "#ffffff60",
"scrollbarSlider.activeBackground": "#ffffff40",
"scrollbarSlider.background": "#ffffff20",
"scrollbarSlider.hoverBackground": "#ffffff60",

## [0.1.0]

- chg selected foregrounds and highlights to blue
- retain v0.0.2 for those who prefer original look

## [0.0.2]

- fix error in pkg, manifest
- adjust panel title contrasts
- darken status bar item hover background
- fix (or recover) side bar section header background

## [0.0.1]

- Initial release
