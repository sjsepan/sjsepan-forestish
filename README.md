# Forestish Theme

Forestish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-forestish_code.png](./images/sjsepan-forestish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-forestish_codium.png](./images/sjsepan-forestish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-forestish_codedev.png](./images/sjsepan-forestish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-forestish_ads.png](./images/sjsepan-forestish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-forestish_theia.png](./images/sjsepan-forestish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-forestish_positron.png](./images/sjsepan-forestish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/14/2025
