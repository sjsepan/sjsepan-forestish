#scratchpad

# install Node Package Manager

sudo apt install npm

# install vsce

npm install -g @vscode/vsce

# package (see https://gitlab.com/sjsepan/repackage-vsix)

vsce-package-vsix.sh

# publish

vsce publish -i ./sjsepan-forestish-0.2.3.vsix
npx ovsx publish sjsepan-forestish-0.2.3.vsix --debug --pat <PAT>
